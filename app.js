const express = require('express')
const app = express()
const port = 3000;
const puppeteer = require('puppeteer');

app.get('/', async (req, res, next) => {
    const url = req.query.url ? req.query.url : null;
    let html = '';
    if(url) {
        try{
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            await page.goto(url);
            html = await page.evaluate(() => document.body.innerHTML);
        } catch(e) {
            next(e);
        }
    }   
    res.send(html);
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))